#include <IRremote.h>

const int PIN = 11;
IRrecv irrecv(PIN);
decode_results results;

void setup() {
	Serial.begin(9600);
	irrecv.enavleIRIn();	///< 初始化红外遥控
	pinMode(12, 1);
}

void loop() {
	// 检测是否接收到信号
	if(irrecv.decode(&results)) {
		Serial.print("Value:");
		Serial.println(results.value);
		irrecv.resume();	///< 接收下一条指令
		delay(100);
	}
}
