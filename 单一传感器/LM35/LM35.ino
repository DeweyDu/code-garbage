/**
 * @author	Kevin@1997
 * @brief	获取室内室外温度
 * @details 控制温度传感器获取温度
 * @date	2020-11-24
 * @version V1.0
 **********************************************************************************
 * @attention
 * @par 修改日志:
 * <table>
 * <tr><th>	    Date    <th>  Version  <th>    Author   <th>  Description
 * <tr><td>  2020-11-24 <td>    1.0    <td>  Kevin@1997 <td>  创建初始版本
 * </table>
 *
 **********************************************************************************
*/

void setup() {
	/**
	 * @brief 初始化端口 端口速率为 9600
	*/
	Serial.begin(9600);
}

int val;
double temp;

void loop() {
	/**
	 * @brief 读取模拟量
	 */
	val = analogRead(0);
	/**
	 * @brief 把模拟量转换为温度
	 */
	temp = turnTemp((double)val);
	/**
	 * @brirf 延时 1000 ms
	 */
	delay(1000);
	/**
	 * @biref 在串口模拟器上打印字符
	*/
	Serial.print("Temperature:");
	Serial.print(temp);
	Serial.println("C");
}

/**
 * @fn 模拟量转换为温度
 * @param value 模拟量
 * @return 返回温度
*/
double turnTemp(double value) {
	return value * (5 / 10.24);
}
