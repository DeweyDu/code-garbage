/**
 * @author 霍文博
 * @email 1684240320@qq.com
 * @version V1.0
 * @date 2020/11/26 19:23
 ****************************************************************
 * @ChangeHistory:
 *     <Date>    |  <Version>  |  <Author>   |  <Description>
 *   2014/01/24  |     V1.0    |   霍文博    |   创建初始版本
 ****************************************************************
*/
void setup() {
	/**
	 * @function pinMode(unsigned int x, OUTPUT)
	 * @description 初始化 x 端口号
	 * @input x 要被初始化的端口号
	 */
	pinMode(12, OUTPUT);
	pinMode(11, OUTPUT);
	pinMode(10, OUTPUT);
}

const int time = 100;	///< 控制间隔时间, 数值越大间隔越长

void loop() {
	/**
	 * @function digitalWrite(int x, LOW : HIGH)
	 * @description 在 x 端口写入 LOW : HIGH
	 * (LOW 为低电平, HIGH 为高电平)
	 */
	digitalWrite(12, LOW);
	digitalWrite(11, LOW);
	digitalWrite(10, LOW);
	/**
	 * @function delay(unsigned int num)
	 * @description 延时一段时间
	 * @input num 控制间隔时间, 数值越大间隔越长
	 */
	delay(time);
	
	digitalWrite(12, HIGH);
	digitalWrite(11, LOW);
	digitalWrite(10, LOW);
	delay(time);
	
	digitalWrite(12, LOW);
	digitalWrite(11, HIGH);
	digitalWrite(10, LOW);
	delay(time);
	
	digitalWrite(12, LOW);
	digitalWrite(11, LOW);
	digitalWrite(10, HIGH);
	delay(time);
	
	digitalWrite(12, HIGH);
	digitalWrite(11, HIGH);
	digitalWrite(10, LOW);
	delay(time);
	
	digitalWrite(12, LOW);
	digitalWrite(11, HIGH);
	digitalWrite(10, HIGH);
	delay(time);
	
	digitalWrite(12, HIGH);
	digitalWrite(11, LOW);
	digitalWrite(10, HIGH);
	delay(time);
	
	digitalWrite(12, HIGH);
	digitalWrite(11, HIGH);
	digitalWrite(10, HIGH);
	delay(time);
}
