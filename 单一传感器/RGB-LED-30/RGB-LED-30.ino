	strip.begin();
	strip.show();
	Serial.begin(9600);
}
void loop() {
	unsigned char red = random(0, 256);		///< 红, 取值范围 { red | [0, 256) }
	unsigned char blue = random(0, 256);	///< 蓝
	unsigned char green = random(0, 256);	///< 绿
	// 随机在 0~29 之间的一个 LED 灯发亮
	strip.setPixelColor(random(0, 30), red, green, blue);
	strip.show();
	delay(50);
}
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
	#include <avr/power.h>
#endif
const int PIN = 7;


Adafruit_NeoPixel strip = Adafruit_NeoPixel(60, PIN, NEO_RGB + NEO_KHZ800);

void setup() {
	/*#if defined (__AVR_ATtiny85__)
		if(F_CPU == 0x7FFFFFFF)
			clock_prescale_set(clock_div_1);
	#endif*/
	clock_prescale_set(0x7FFFFFFF);
