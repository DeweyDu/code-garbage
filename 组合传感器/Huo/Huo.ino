#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Adafruit_NeoPixel.h>
/*#ifdef __AVR__
	#include <avr/power.h>
#endif*/

const int PIN = 8;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(60, PIN, NEO_RGB + NEO_KHZ800);

const char trgPin = 13;
const char ecoPin = A0;

LiquidCrystal_I2C lcd(0x20,16,2);

float dist;

void setup() {
	lcd.init(); 		//初始化LCD
	strip.begin();
	strip.show();
	pinMode(trgPin, OUTPUT);
	pinMode(ecoPin, INPUT);
}

void loop() {
	strip.setPixelColor((int)dist % 30, 0, 0, 0);
	strip.show();

	digitalWrite(trgPin, HIGH);
	delayMicroseconds(10);
	digitalWrite(trgPin, LOW);
	
	dist = pulseIn(ecoPin, HIGH) * 0.034 / 2;
	
	lcd.setCursor(0,0);
	lcd.print("Dist:");
	lcd.print(dist);
	lcd.print("CM  ");
	
	strip.setPixelColor((int)dist % 30, 0, 255, 0);
	strip.show();
	delay(1000);
}
